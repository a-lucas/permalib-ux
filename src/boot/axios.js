import Vue from 'vue'
import axios from 'axios'
// const baseUrl = `${location.protocol}//${location.hostname}:9000`;
//
// Vue.prototype.$axios = axios


export const baseURL = process.env.API;


const axiosInstance = axios.create({
  baseURL,
  timeout: 3000000,
  //adapter: throttleAdapterEnhancer(cacheAdapterEnhancer(axios.defaults.adapter, {defaultCache: new LRUCache({ maxAge: FIVE_MINUTES })})),
});


let axiosStoreSetup = false;

const setUp = (store) => {
  if (!axiosStoreSetup) {
    axiosStoreSetup = true;
  } else {
    return;
  }

  axiosInstance.interceptors.request.use(function (config) {
    store.commit(`common/INCR_AJAX`);
    console.log('baseURL', baseURL, 'requesting ', config)
    return config;
  }, function (error) {
    store.commit(`common/DECR_AJAX`);
    // store.dispatch(`common/${Actions.DECR_AJAX}`);
    return Promise.reject(error);
  });

// Add a response interceptor
  axiosInstance.interceptors.response.use(function (response) {
    store.commit(`common/DECR_AJAX`);
    // store.dispatch(`common/${Actions.DECR_AJAX}`);
    return response;
  }, function (error) {
    store.commit(`common/DECR_AJAX`);
    // store.dispatch(`common/${Actions.DECR_AJAX}`);
    return Promise.reject(error);
  });

}

const token = localStorage.getItem('user-token');
if (token) {
  axiosInstance.defaults.headers.common['Authorization'] = token;
}

export default (params) => {
  const {Vue, store} = params;
  setUp(store);
  Vue.prototype.$axios = axiosInstance;
  Vue.prototype.$baseURL = baseURL;
}
