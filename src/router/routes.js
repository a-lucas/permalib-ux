
const routes = [
  {
    path: '/:leftPane/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') , name: 'index'},
      { path: 'search', component: () => import('pages/Search.vue'), name: 'search' },
      { path: 'video/:id', component: () => import('pages/Video.vue'), name: 'video' },

    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
