import Vue from 'vue'
import Vuex from 'vuex'

import video from './video/index';
import {VideoStore} from './shortcuts';
import actions from "src/store/video/actions";
import common from './common/index';

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const store = new Vuex.Store({
    modules: {
      video,
      common
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  });

  store.dispatch(`video/${actions.LOAD_CHANNELS}`).then(() =>
    store.dispatch(`video/${actions.LOAD_SEARCHES}`).then(() => {})
  );
  store.dispatch(`video/${actions.LOAD_FARMS}`)
  store.dispatch(`video/${actions.LOAD_PLANTS}`)

  return store
}

export {VideoStore, actions};
