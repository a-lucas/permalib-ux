const baseUrl = process.env.API;

const POSTData = data => {
  console.log('POST', data);
  return {
    method: 'post',
    body: JSON.stringify(data),
    headers: {
      'accept': 'application/json',
      'Accept-Language': 'en-US,en;q=0.8',
      'Content-Type': 'application/json',
    },
  }
};
const PUTData = data => ({
  method: 'put',
  body: JSON.stringify(data),
  headers: {
    'accept': 'application/json',
    'Accept-Language': 'en-US,en;q=0.8',
    'Content-Type': 'application/json',
  },
});

const actions = {
  NEW_SCREENSHOT: 'NEW_SCREENSHOT',
  SAVE_SCREENSHOT: 'SAVE_SCREENSHOT',
  SET_CURRENT_SCREENSHOT: 'SET_CURRENT_SCREENSHOT',
  REMOVE_SCREENSHOT: 'REMOVE_SCREENSHOT',

  DOWNLOAD_VIDEO: 'DOWNLOAD_VIDEO',
  DOWNLOAD_ALL_VIDEOS: 'DOWNLOAD_ALL_VIDEOS',
  LOAD_CURRENT_VIDEO: 'LOAD_CURRENT_VIDEO',
  LOAD_NEW_DOWNLOADED_VIDEOS: 'LOAD_NEW_DOWNLOADED_VIDEOS',
  SET_TIME_SPENT_ON_VIDEO: 'SET_TIME_SPENT_ON_VIDEO',

  LOAD_FARMS: 'LOAD_FARMS',
  NEW_FARM: 'NEW_FARM',
  SAVE_FARM: 'SAVE_FARM',
  SET_VIDEO_FARM: 'SET_VIDEO_FARM',

  LOAD_SEARCHES: 'LOAD_SEARCHES',
  MAKE_SEARCH: 'MAKE_SEARCH',

  LOAD_PLANTS: 'LOAD_PLANTS',
  ADD_PLANT_TO_SCREENSHOT: 'ADD_PLANT_TO_SCREENSHOT',
  REMOVE_PLANT_FROM_SCREENSHOT: 'REMOVE_PLANT_FROM_SCREENSHOT',

  NEW_MEASUREMENTS: 'NEW_MEASUREMENTS',
  SAVE_MEASUREMENTS: 'SAVE_MEASUREMENTS',
  DELETE_MEASUREMENTS: 'DELETE_MEASUREMENTS',

  LOAD_CHANNELS: 'LOAD_CHANNELS'
}

export default actions;

const failedFetch = console.error

const storeActions = {

  // FARMS
  [actions.LOAD_FARMS]: ({commit}) => {
    return fetch(`${baseUrl}/farm/all`)
      .then(res => res.json())
      .then(data => commit(actions.LOAD_FARMS, data))
      .catch(failedFetch)
  },
  [actions.NEW_FARM]: ({commit}, farm) => {
    return fetch(`${baseUrl}/farm/new`, POSTData(farm))
      .then(res => res.json())
      .then(data => {
        commit(actions.NEW_FARM, data);
        return data;
      })
      .catch(failedFetch)
  },
  [actions.SAVE_FARM]: ({commit}, farm) => {
    return fetch(`${baseUrl}/farm/save`, PUTData(farm))
      .then(res => res.json())
      .then(data => commit(actions.SAVE_FARM, data))
      .catch(failedFetch)
  },
  [actions.SET_VIDEO_FARM]: ({commit}, {video_id, farm_id}) => {
    return fetch(`${baseUrl}/video/setFarm?videoId=${video_id}&farmId=${farm_id}`)
      .then(res => res.json())
      .then(video => commit(actions.SET_VIDEO_FARM, video))
      .catch(failedFetch)
  },

  // MEASUREMENTS

  [actions.NEW_MEASUREMENTS]: ({commit}, measurement) => {
    return fetch(`${baseUrl}/measurement/new`, POSTData(measurement))
      .then(res => res.json())
      .then(data => {
        commit(actions.NEW_MEASUREMENTS, data)
        return data;
      })
      .catch(failedFetch)
  },
  [actions.SAVE_MEASUREMENTS]: ({commit}, measurement) => {
    commit(actions.SAVE_MEASUREMENTS, measurement);
    return fetch(`${baseUrl}/measurement/save`, PUTData(measurement))
      .catch(failedFetch)
  },
  [actions.DELETE_MEASUREMENTS]: ({commit}, measurementId) => {
    return fetch(`${baseUrl}/measurement/delete?id=${measurementId}`)
      .then(res => res.json())
      .then(data => {
        commit(actions.DELETE_MEASUREMENTS, measurementId);
        return data;
      })
      .catch(failedFetch)
  },

  // VIDEO
  [actions.DOWNLOAD_ALL_VIDEOS]: ({commit, state}) => {
    const ids = Object.values(state.searchResults)
      .filter(({video_uid, downloaded, downloading}) => video_uid.length>0 && !downloaded && ! downloading)
      .map(({video_uid}) => video_uid);

    return fetch(`${baseUrl}/video/downloadmany?ids=${ids.join(',')}`)
      .then(res => res.json())
      .then(data => commit(actions.DOWNLOAD_ALL_VIDEOS, data))
      .catch(failedFetch)
  },
  [actions.SET_TIME_SPENT_ON_VIDEO]: ({commit}, {video_id, time_spent}) => {
    return fetch(`${baseUrl}/video/timeSpent?videoId=${video_id}&timeSpent=${time_spent}`)
      .then(res=> res.json())
      .then(data => commit(actions.SET_TIME_SPENT_ON_VIDEO, data))
      .catch(failedFetch)
  },

  // CURRENT VIDEO
  [actions.LOAD_CURRENT_VIDEO]: ({commit}, videoId) => {
    return fetch(`${baseUrl}/video/load?id=${videoId}`)
      .then(res => res.json())
      .then(data => commit(actions.LOAD_CURRENT_VIDEO, data))
      .catch(failedFetch)
  },

  // SEARCHES
  [actions.LOAD_SEARCHES]: ({commit}) => {
    return fetch(`${baseUrl}/search/all`)
      .then(res => res.json())
      .then(data => commit(actions.LOAD_SEARCHES, data))
      .catch(failedFetch)
  },
  [actions.MAKE_SEARCH]: ({commit, dispatch}, {keyword, channel}) => {
    if (keyword.length > 0 || channel.length > 0) {
      return fetch(`${baseUrl}/search/make?keyword=${encodeURIComponent(keyword)}&channel=${encodeURIComponent(channel)}`)
        .then(res => res.json())
        .then(data => commit(actions.MAKE_SEARCH, data))
        .then(dispatch(actions.LOAD_SEARCHES))
        .catch(failedFetch)
    }
    return new Promise()
  },

  // SCRAPPING
  [actions.DOWNLOAD_VIDEO]: ({commit}, youtubeVideoId) => {
    return fetch(`${baseUrl}/video/download?id=${youtubeVideoId}`)
      .then(res => res.json())
      .then(data => commit(actions.DOWNLOAD_VIDEO, data))
      .catch(failedFetch)
  },

  // CHANNELS
  [actions.LOAD_CHANNELS]: ({commit}) => {
    return fetch(`${baseUrl}/channels/all`)
      .then(res => res.json())
      .then(data => commit(actions.LOAD_CHANNELS, data))
      .catch(failedFetch)
  },

  //SCREENSHOTS
  [actions.NEW_SCREENSHOT]: (({commit}, data) => {
    return fetch(`${baseUrl}/screenshot/new`, POSTData(data))
      .then(res => res.json())
      .then(data => commit(actions.NEW_SCREENSHOT, data))
      .catch(failedFetch)
  }),
  [actions.SAVE_SCREENSHOT]: ({commit}, screenshot) => {
    return fetch(`${baseUrl}/screenshot/save`, PUTData(screenshot))
      .then(res => res.json())
      .then(data => commit(actions.SAVE_SCREENSHOT, data))
      .catch(failedFetch)
  },
  [actions.REMOVE_SCREENSHOT]: ({commit}, screenshot) => {
    return fetch(`${baseUrl}/screenshot/delete?screenshot_id=${screenshot.id}`)
      .then(res => res.json())
      .then(data => commit(actions.REMOVE_SCREENSHOT, data))
      .catch(failedFetch)
  },

  //PLANTS
  [actions.LOAD_PLANTS]: ({commit}) => {
    return fetch(`${baseUrl}/plant/allplants`)
      .then(res => res.json())
      .then(data => commit(actions.LOAD_PLANTS, data))
      .catch(failedFetch)
  },
  [actions.ADD_PLANT_TO_SCREENSHOT]: ({commit}, {screenshot_id, plant_id}) => {
    return fetch(`${baseUrl}/screenshot/addplant?plant_id=${plant_id}&screenshot_id=${screenshot_id}`)
      .then(res => res.json())
      .then(data => commit(actions.ADD_PLANT_TO_SCREENSHOT, data))
      .catch(failedFetch)
  },
  [actions.REMOVE_PLANT_FROM_SCREENSHOT]: ({commit}, {screenshot_id, plant_id}) => {
    return fetch(`${baseUrl}/screenshot/removeplant?plant_id=${plant_id}&screenshot_id=${screenshot_id}`)
      .then(res => res.json())
      .then(data => commit(actions.REMOVE_PLANT_FROM_SCREENSHOT, data))
      .catch(failedFetch)
  }
};

export {storeActions};
