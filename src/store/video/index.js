import initialState from "src/store/video/initial";
import getters from './getters';
import actions, {storeActions} from './actions';
import Vue from 'vue'

export default {
  namespaced: true,
  state: initialState,
  getters,
  mutations: {
    // Farms
    [actions.LOAD_FARMS]: (state, farms) => {
      state.farmIndex = farms.reduce((acc, farm) => Object.assign({}, acc, {[farm.id]: farm} ));
    },
    [actions.NEW_FARM]: (state, farm) => {
      state.farmIndex = Object.assign({}, state.farmIndex,  {[farm.id]: farm} );
    },
    [actions.SAVE_FARM]: (state, farm) => {
      state.farmIndex = Object.assign({}, state.farmIndex,  {[farm.id]: farm} );
    },
    [actions.SET_VIDEO_FARM]: (state, {Video, Farm}) => {
      state.farmIndex = Object.assign({}, state.farmIndex,  {[Farm.id]: Farm} );
      state.videoIndex = Object.assign({}, state.videoIndex,  {[Video.id]: Video} );
      state.currentVideo = Video;
      console.log('Linked Farm: ', Farm)
    },

    // Search
    [actions.MAKE_SEARCH]: (state, results) => {
      state.searchResults = results;
      console.log(state.searchResults)
    },
    [actions.LOAD_SEARCHES]: (state, searches) => state.searches = searches,

    // Current Video
    [actions.LOAD_CURRENT_VIDEO]: (state, video) => {
      state.currentVideo = Object.freeze(video);
    },
    [actions.SET_TIME_SPENT_ON_VIDEO]: (state, video) => {
      state.currentVideo =Object.freeze(video);
    },
    // Screenshots
    [actions.NEW_SCREENSHOT]: (state, screenshot) => {
      console.log('state.crurentVideo', state.currentVideo);
      console.log('edges', state.currentVideo.edges)
      Vue.set(state.currentVideo.edges, 'Screenshots', state.currentVideo.edges.Screenshots.concat([screenshot]));

      state.currentScreenshot = screenshot;
    },
    [actions.SAVE_SCREENSHOT]: (state, screenshot) => {
      const screenShotIndex = state.currentVideo.edges.Screenshots.findIndex(({id}) => screenshot.id === id);
      if (screenShotIndex===-1) {
        throw new Error('not found')
      }
      console.log('ScreenShot Index', screenShotIndex)
      Vue.set(state.currentVideo.edges.Screenshots, screenShotIndex, screenshot);
      state.currentScreenshot = screenshot;
    },
    [actions.SET_CURRENT_SCREENSHOT]: (state, screenshot) => {
      state.currentScreenshot = screenshot;
    },
    [actions.REMOVE_SCREENSHOT]: (state, id) =>{
      state.currentScreenshot = null;
      const newScreenshotList = state.currentVideo.edges.Screenshots.filter(s => s.id != id)
      Vue.set(state.currentVideo.edges, 'Sreenshots', newScreenshotList)
    },
    //Scrapping
    [actions.DOWNLOAD_VIDEO]: (state, video) => {
      console.log(state, state.searchResults)
      state.searchResults = state.searchResults.map(vid => vid.id === video.id ? video : vid);
    },

    //Channels
    [actions.LOAD_CHANNELS]: (state, channels) => {
      state.channelIndex = channels.reduce((acc, channel) => Object.assign({}, acc, {[channel.youtube_id]: channel}), {})
      console.log('channels: ', state['channelIndex']);
    },

    // Plants

    [actions.LOAD_PLANTS]: (state, plants) => {
      state.plants = plants;
    },
    [actions.ADD_PLANT_TO_SCREENSHOT]: (state, plants) => {
      Vue.set(state.currentScreenshot.edges, 'Plant', plants);
    },
    [actions.REMOVE_PLANT_FROM_SCREENSHOT]: (state, plants) => {
      Vue.set(state.currentScreenshot.edges, 'Plant', plants);
    },

    // Measurements
    [actions.NEW_MEASUREMENTS]: (state, data) => {
      state.measurementIndex = Object.assign({}, state.measurementIndex, {[data.id]: data})
      const size =  state.currentScreenshot.edges.Measurements ? state.currentScreenshot.edges.Measurements.length: 0;
      if (size ===0) {
        Vue.set(state.currentScreenshot.edges, 'Measurements', [data])
      } else {
        Vue.set(state.currentScreenshot.edges.Measurements, size, data)
      }
      console.log('NEW MEASUREMENT', state.currentScreenshot)
    },
    [actions.SAVE_MEASUREMENTS]: (state, data) => {
      state.measurementIndex = Object.assign({}, state.measurementIndex, {[data.id]: data})
      const index = state.currentScreenshot.edges.Measurements.findIndex(ss => ss.id===data.id)
      if (index ===-1) {
        Vue.set(state.currentScreenshot.edges, 'Measurements', [data])
      } else {
        Vue.set(state.currentScreenshot.edges.Measurements, index, data)
      }
      console.log('SAVE MEASUREMENT', state.currentScreenshot)
    },
    [actions.DELETE_MEASUREMENTS]: (state, id) => {
      Vue.delete(state.measurementIndex, id);
    },
  },
  actions: storeActions
}
