
export default {
  searches: (state, getters) =>{
    return state.searches;
  },
  search_results:(state, getters) => {
    return state.searchResults;
  },
  current_video: state => state.currentVideo,
  current_video_url: state => videoId => {

    if (state.currentVideo.nb_files === 1) {
      return [
        {
          url: `${process.env.API}/videos/${videoId}/video_0.mp4`,
          type: state.currentVideo.video_mime.split(';')[0]
        },
      ];
    } else if (state.currentVideo.nb_files === 2) {
      return [
        {
          url: `${process.env.API}/videos/${videoId}/video.mp4`,
          type: state.currentVideo.video_mime.split(';')[0]
        },
        // {
        //   url: `${location.protocol}//${location.hostname}:9000/videos/${videoId}/video_1.mp4`,
        //   type: state.currentVideo.audio_mime.split(';')[0]
        // },
      ];
    } else {
      console.error('Video not downloaded yet')
      return [];
    }


  },
  select_queries: state => state.searches
    .filter(({search_query}) => search_query && search_query.length > 0 ),
  select_channels: state => state.searches
    .filter(({search_channel}) => search_channel && search_channel.length>0)
    .map(({search_channel}) => state.channelIndex[search_channel]),
  channel_index: state => state.channelIndex,
  current_screenshot: state => state.currentScreenshot,
  select_farms: state => Object.values(state.farmIndex),
  farms_index: state => state.farmIndex,
  farms: state => Object.values(state.farmIndex).sort((a, b) => {
    return a.id-b.id
  }),
  plants: state=> Object.freeze(state.plants.map(plant=> {
    return {
      label: plant.Name,
      id: plant.Id,
      children: plant.Children.map(plantCat => ({
        label: plantCat.Name,
        id: plantCat.Id,
      }))
    }
  })),
  plantIndex: state => state.plants.reduce((acc, plant) => Object.assign({}, acc, {[plant.Name]: plant}), {}),
  measurementIndex: state => state.measurementIndex,
  measurements: state => Object.values(state.measurementIndex).sort((a, b) => a.id-b.id),
  /*
  [
        {
          label: 'Satisfied customers (with avatar)',
          avatar: 'https://cdn.quasar.dev/img/boy-avatar.png',
          children: [
            {
              label: 'Good food (with icon)',
              icon: 'restaurant_menu',
              children: [
                { label: 'Quality ingredients' },
                { label: 'Good recipe' }
              ]
            },
   */
}

