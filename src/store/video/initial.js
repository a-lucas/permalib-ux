export default  {
  farmIndex: {},
  videoIndex: {}, // video object indexed by video.id
  currentVideo: {},
  searchResults: [],
  searches: [],
  channelIndex: {},
  currentScreenshot: null,
  plants: [],
  plantIndex: {},
  measurementIndex: {}
};
