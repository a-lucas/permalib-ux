const getters = {
  ajaxCounter: state => state.ajaxCounter
};

const initialState = {
  ajaxCounter: 0,
};


export {initialState, getters};

