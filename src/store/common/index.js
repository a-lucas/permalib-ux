import {initialState, getters} from './initial';
import Actions from './actions';

import {
  Loading,
  QSpinnerGears
} from 'quasar';

export default {
  namespaced: true,
  state: initialState,
  getters: getters,
  actions: {
    [Actions.DECR_AJAX]: ({commit}) => {
      setTimeout(() => {
        commit(Actions.DECR_AJAX)
      }, 300);
    },
  },
  mutations: {
    [Actions.INCR_AJAX]: (state) => {
      state.ajaxCounter++;
      // console.log('Ajax counter', state.ajaxCounter);
      Loading.show({
        delay: 0,
        spinner: QSpinnerGears,
      })
    },
    [Actions.DECR_AJAX]: (state) => {
      state.ajaxCounter--;
      // console.log('Ajax counter', state.ajaxCounter);
      if (state.ajaxCounter ===0) {
        Loading.hide();
      }
    },
  },
}
