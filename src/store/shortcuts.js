import {createNamespacedHelpers} from 'vuex';

export const VideoStore = args => {
  const {mapGetters} = createNamespacedHelpers('video');
  return mapGetters(args);
};
