export const  panelTypes = [
  {
    label: 'Registered Farms',
    value: 'farms',
    getterName: 'farms'
  },
  {
    label: 'Identified Plants',
    value: 'plants',
    getterName: 'plants'
  },
  {
    label: 'Common Searches',
    value: 'searches',
    getterName: 'searches'
  },
  {
    label: 'Most active Videos',
    value: 'activeVideos',
  },
  {
    label: 'New Downloaded Videos',
    value: 'video',
    getterName: 'newVideos'
  },
]
