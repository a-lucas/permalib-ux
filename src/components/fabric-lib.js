import {fabric} from "fabric";
import {baseURL} from "boot/axios";

export const DrawingMode = {
  Line: 0,
  Point: 1,
}

const CursorMode  = {
  Draw: 'Draw',
  Select: 'Select'
}

class PointDrawer {

  drawingMode = DrawingMode.Point;
  step=1;
  origin;
  end;
  line;
  id;
  lineWidth = 10;
  radius = 20;
  onSave = []


  constructor(canvas) {
    this.canvas = canvas;
    this.step=1;
  }

  New(cbs = []) {
    console.log('enabling ', this.drawingMode)
    this.canvas.on('mouse:down', this.mouseDown);
    this.canvas.on('mouse:up', this.mouseUp);
    this.step = 1;
    this.onSave = cbs;
  }

  Draw({x1, y1, x2, y2, id}, cb) {

    this.step = 3;
    this.onSave = cb;

    this.origin = this._newCircle(y1, x1, false);
    this.end = this._newCircle(y2, x2, false);
    this.line = new fabric.Line([x1, y1, x2, y2], {
      strokeWidth: this.lineWidth,
      fill: '#fff',
      stroke: '#fff',
      opacity: 0.85,
      selectable: false,
      strokeUniform: true,
    });
    this.line.hasControls = this.line.hasBorders = false;
    this.line.source = this.origin.source = this.end.source = this.drawingMode;
    this.origin.name = 'origin';
    this.end.name = 'end';

    this.canvas.add(this.origin);
    this.canvas.add(this.end);
    this.canvas.add(this.line);
    return {
      line: this.line,
      origin: this.origin,
      end: this.end,
      id: this.id,
    }
  }

  Load(input, cb) {
    console.log('Load with ', input)
    const {line, origin, end, id} = input;
    this.id = id;
    this.step = 3;
    this.onSave = cb;

    this.origin = origin;
    this.end = end;
    this.line = line;
    this.origin.selectable = true;
    this.end.selectable = true;
    this.origin.stroke = '#aa1841';

    this.origin.setOptions({
      fill: '#aa1841',
      // stroke: '#aa1841'
    })
    this.end.setOptions({
       fill: '#aa1841',
      // stroke: '#aa1841'
    })
    this.line.setOptions({
      fill: '#aa1841',
       stroke: '#aa1841'
    })


    console.log('this class ', this)

    this.canvas.on('mouse:down', this.mouseDown);
    this.canvas.on('mouse:up', this.mouseUp);
    this.canvas.on('object:moving', this.objectMoving);

    this.canvas.renderAll();
  }

  save() {
    return this.onSave.map(cb => cb(
      this.toJSON(),
      {origin: this.origin, line: this.line, end: this.end, id: this.id},
      id => this.id = id));
  }

  reset(){
    this.canvas.off('mouse:down', this.mouseDown);
    this.canvas.off('mouse:up', this.mouseUp);
    this.canvas.off('object:moving', this.objectMoving);
    this.canvas.off('mouse:move', this.mouseMove);
    this.origin.selectable = false;
    this.end.selectable =false;
    this.origin.setOptions({
      fill: '#fff',
    })
    this.end.setOptions({
      fill: '#fff',
    })
    this.line.setOptions({
      fill: '#fff',
      stroke: '#fff'
    })
    this.canvas.renderAll();
  }

  mouseUp = o => {
    if (this.step ===3) {
      this.save();
    }
  }

  mouseDown = (o) => {
    const {x, y} = this.canvas.getPointer(o.e);
    console.log('mouse Down: ', x, y)
    switch (this.step) {
      case 1:
        this.origin = this._newCircle(y, x, true);
        this.end = this._newCircle(y, x, true);
        this.line = new fabric.Line([x, y, x, y], {
          strokeWidth: this.lineWidth,
          fill: '#fff',
          stroke: '#fff',
          opacity: 0.85,
          selectable: false,
          strokeUniform: true,
        });
        this.line.hasControls = this.line.hasBorders = false;

        this.line.source = this.origin.source = this.end.source = this.drawingMode;
        this.origin.name = 'origin';
        this.end.name = 'end';

        this.canvas.add(this.origin);
        this.canvas.add(this.end);
        this.canvas.add(this.line);
        this.canvas.on('mouse:move', this.mouseMove);
        this.step++;
        break;
      case 2:
        console.log('removing event listener')
        this.mouseMove(o);
        this.step++;
        this.canvas.off('mouse:move', this.mouseMove);
        console.log('Activate object moving listener')
        this.canvas.on('object:moving', this.objectMoving);
        break;
      default:
        break;
    }
  }

  objectMoving = (o) => {
    // const {x, y} = this.canvas.getPointer(o.e);
    switch (this.step) {
      case 3:
        const p = o.target;
        // console.log('source', p.source, this.drawingMode);
        if (p.source === this.drawingMode) {
          switch (p.name) {
            case 'origin':
              this.line.set({x1: p.left+20, y1: p.top+20})
              break;
            case 'end':
              this.line.set({x2: p.left+20, y2: p.top+20})
              break;
            default:
              console.log('This dom has no name', p)
              break;
          }
          this.canvas.renderAll();
        } else {
          console.log('source unknown')
        }
        break;
      default:
        console.log('object move step = ', this.step);
        break;
    }
  }

  toJSON() {
    return {
      id: this.id,
      x1: this.origin.left,
      y1: this.origin.top,
      x2: this.end.left,
      y2: this.end.top,
    }
  }

  mouseMove= (o) => {
    const {x, y} = this.canvas.getPointer(o.e);
    switch (this.step) {
      case 2:
          this.line.set({
            x2: x-this.lineWidth/2,
            y2: y-this.lineWidth/2
          }).setCoords();

          this.end.set({
            left: x-this.radius,
            top: y-this.radius
          }).setCoords();
          this.canvas.renderAll();
        break;
      default:
        console.log('mouse move step = ', this.step);
        break;
    }
  }

  _newCircle(top, left, selectable) {
    const options = {
      left: parseInt(left, 10)-this.radius,
      top: parseInt(top)-this.radius,
      strokeWidth: 2,
      radius: this.radius-2,
      fill: '#fff',
      stroke: '#666',
      opacity: 0.85,
      selectable,
    };

    const c = new fabric.Circle(options);
    c.hasControls = c.hasBorders = false;
    return c;
  }

}

class LineDrawer {
  drawingMode = DrawingMode.Line;


  constructor(canvas) {
    this.canvas = canvas
  }

  activate() {}

  reset(){};

  make(x, y, options, x2, y2) {
    return new fabric.Line([x, y, x2, y2], options)
  }

  resize(object, x, y) {
    console.log('resizing LIne', object)
    //Change the secondary point (x2, y2) of the object
    //This resizes the object between starting point (x,y)
    //and secondary point (x2,y2), where x2 and y2 have new values.
    object.set({
      x2: x,
      y2: y
    }).setCoords();
    //Wrap the resized object in a Promise
    return object
  }

}

class DrawingEditor {

  _drawer; //Current drawer
  drawerOptions; //Current drawer options
  drawers = []; //All possible drawers
  object; //The object currently being drawn
  fabric;
  canvasContainer;
  bgImg;
  isDown;
  cursorMode;

  measurementIndex = {};

  constructor(canvas, canvasContainer, imgId) {
    this.fabric = new fabric.Canvas(canvas, {selection: true});
    this.canvasContainer = canvasContainer;
    this.cursorMode = CursorMode.Draw;
    const {offsetWidth, offsetHeight} = this.canvasContainer;
    this.exp(offsetHeight, offsetWidth, imgId);
    window.addEventListener('resize', () => {
      const {offsetWidth, offsetHeight} = this.canvasContainer
      this.resize(offsetHeight, offsetWidth)
    }, false);

    this.drawers = [new LineDrawer(this.fabric), new PointDrawer(this.fabric)];
    this._drawer = this.drawers[DrawingMode.Line];
  }

  fullScreen() {

    const c  = document.createElement('canvas')
    c.style.position = 'fixed';
    c.style.left='0px';
    c.style.top='0px';
  }

  newDrawer(drawingType, cb) {
    if (this._drawer) {
      this._drawer.reset();
    }
    this._drawer = this.drawers[drawingType];
    this._drawer.New([cb, (data, second) => {
      console.log('newDrawer CB', data, second)
      this.measurementIndex[data.id] = second;
    }]);
  }

  drawDrawer(drawingType, data, cb) {
    if (this._drawer) {
      this._drawer.reset();
    }
    this._drawer = this.drawers[drawingType];
    switch (drawingType) {
      case DrawingMode.Point:
        this.measurementIndex[data.id] = Object.assign({}, this._drawer.Draw(data, [cb]), {id: data.id});
        break;
      default:
        console.error('case unset ', drawingType)
    }
  }

  loadDrawer(drawingType, id, cb) {
    if (this._drawer) {
      this._drawer.reset();
    }
    console.log('Loading Drawer', id)
    this._drawer = this.drawers[drawingType];
    switch (drawingType) {
      case DrawingMode.Point:
        this._drawer.Load(this.measurementIndex[id], [cb, (data, second) => {
          console.log('loadDrawer CB', data, second)
        }]);
        break;
      default:
        console.error('case unset ', drawingType)
    }
  }

  resize(h, w) {

    const scaleRatio = w/h

    this.fabric.setWidth(w)
    this.fabric.setHeight(h)
    const canvasAspect = w / h;
    let scaleFactor = 1;
    let generalCanvasRatio = this.bgImg.width / this.bgImg.height;
    if (canvasAspect > generalCanvasRatio) {
      scaleFactor = h / this.bgImg.height;
    } else {
      scaleFactor = w / this.bgImg.width;
    }
    const f_img = new fabric.Image(this.bgImg);
    this.fabric.setBackgroundImage(f_img, this.fabric.renderAll.bind(this.fabric), {
      top: 0,
      left: 0,
      originX: 'left',
      originY: 'top',
      // scaleX: scaleFactor,
      //  scaleY: scaleFactor
    });
    this.fabric.setZoom(scaleFactor)
    this.fabric.renderAll();
  }

  exp(h, w, imageId) {
    this.bgImg = new Image();
    this.bgImg.onload = () => {
      this.resize(h, w);
    };
    this.bgImg.src = `${baseURL}/images/${imageId}.png`;
  }
}


export {DrawingEditor}
